import React, { Fragment, useContext } from 'react';
//import necessary components
import Navbar from 'react-bootstrap/Navbar';//export default = it can be change or placed it on any variable we want
import { Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'

export default function NavBar() {

//The useHistory hook gives you access to the history instance that you may use to navigate/to access a specific location
//push(path) pushes a new entry onto the history stack
	const history = useHistory();

	//useContext() is a react hook used to unwrap our context. It will return the dsata passed as values by a provider
	const { user, setUser, unsetUser } = useContext(UserContext);
	console.log(user)


	const logout = () =>{
		unsetUser();
		setUser({ accessToken: null });
		history.push('/login')
	}

	let rightNav = (user.accessToken !== null ) ? 
		<Fragment>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
		</Fragment>
		 :
		<Fragment>
			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		</Fragment>


	return(
	<Navbar bg="light" expand="lg">
		<Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav"/>
		<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="mr-auto">
				<Nav.Link as={NavLink} to="/">Home</Nav.Link>
				<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>	
			</Nav>
			<Nav className="ml-auto">
				{rightNav}
			</Nav>
		</Navbar.Collapse>
	</Navbar>

		)
}
