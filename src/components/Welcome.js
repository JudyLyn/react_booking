import React from 'react';


export default function Welcome(props){
	return(
		<>
		<h1>Hello! {props.kahitAno}, {props.nickname}</h1>
		<h2>Age: {props.age}</h2>
		<p>Welcome to our Course Booking</p>
		</>
		)
}