//Create a login page

//Activity
//Using the codes from the Register page component, create a Login.js in the pages folder and create a page that simulates the login using an email and password.
//Update the App.js so that the Login page is shown.
//use sweet alert
//disable the button if the email and password is blank
//push your files to gitlab
//copy and paste the link of your gitlab project to boodle named: React Js - Effects, Events and Forms
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

export default function Login(){

	const history = useHistory();

	//consume the UserContext object in the Login page via useContext()
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const[loginButton, setLoginButton] = useState(false);

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setLoginButton(true)
		}else{
			setLoginButton(false)
		}
	}, [email, password])


//API INTEGRATION WITH FETCH
//We will add fetch in login page
//syntax:
//fetch("url", {options})
//url = the URL access from API
//options = optional parameters: method, headers, authorization etc.

//Without options, this is a simple GET request, downloading the contents of the url.
//process: The browser starts the request right away and returns a promise that the calling code should use to get the result

//getting a response is usually a two-stage process
//.then(response => response.json()) //parse the response as JSON object
//.then(data => { console.log(data)}) //process the result that we want to show






	function login(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			//let's do the response in our data once we receive it
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					title: 'Yaaaaaay!',
					icon: 'success',
					text: 'Thank you for loggin in to Zuitt Booking System'
				});


				//get user's details from our token
				fetch('http://localhost:4000/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					//we will check if the user is Admin or not,
					//if admin, we will redirect to the /courses. 
					//if not, redirect to homepage.

					if(data.isAdmin === true ){
						localStorage.setItem('email', data.email);
						localStorage.setItem('isAdmin', data.isAdmin);

						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})
						//what router can we use to redirect the page to /courses if isAdmin is true?
						history.push('/courses')
					}else{
						//isAdmin false - redirect to homepage
						history.push('/')
					}


				})


			}else{
				Swal.fire({
					title: 'Oooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials'
				})
			}
			setEmail('');
			setPassword('');


		})
	}


	//Let's update again the Login.js component to make it redirect to the homepage when it is detected that there is a user currently logged in.
	/*if(user.accessToken !== null) {
		return <Redirect to="/" />
	}*/
	//If the user.email has a value, instead of showing the Login component, go to the route indicated in the "to" prop of Redirect component

	return(
	<Form onSubmit={(e) => login(e)}>
		<Form.Group controlId="userEmail">
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		</Form.Group>

		<Form.Group controlId="password">
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		</Form.Group>
		{loginButton ? 
			<Button variant="success" type="submit">Submit</Button>
			: 
			<Button variant="success" type="submit" disabled>Submit</Button>

		}
		
	</Form>
		)
}